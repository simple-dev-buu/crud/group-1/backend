import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BranchService {
  constructor(
    @InjectRepository(Branch)
    private readonly branchRepository: Repository<Branch>,
  ) {}

  create(createBranchDto: CreateBranchDto) {
    return this.branchRepository.save(createBranchDto);
  }

  findAll() {
    return this.branchRepository.find();
  }

  findOne(id: number) {
    return this.branchRepository.findOneBy({ id: id });
  }

  update(id: number, updateBranchDto: UpdateBranchDto) {
    return this.branchRepository.update(id, updateBranchDto);
  }

  async remove(id: number) {
    await this.branchRepository.delete(id);
  }
}
