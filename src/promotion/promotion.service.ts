import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private readonly promotionRepository: Repository<Promotion>,
  ) {}

  create(createPromotionDto: CreatePromotionDto) {
    return this.promotionRepository.save(createPromotionDto);
  }

  findAll() {
    return this.promotionRepository.find();
  }

  findOne(id: number) {
    return this.promotionRepository.findOneBy({ id: id });
  }

  update(id: number, updatePromotionDto: UpdatePromotionDto) {
    return this.promotionRepository.update(id, updatePromotionDto);
  }

  async remove(id: number) {
    await this.promotionRepository.delete(id);
  }
}
