import { PartialType } from '@nestjs/mapped-types';
import { CreatePromotionDto } from './create-promotion.dto';

export class UpdatePromotionDto extends PartialType(CreatePromotionDto) {
  name: string;
  description: string;
  discount: number;
  startDate: string;
  endDate: string;
  status: 'active' | 'inactive';
}
