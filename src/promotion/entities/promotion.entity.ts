import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  discount: number;

  @Column()
  startDate: string;

  @Column()
  endDate: string;

  @Column()
  status: 'active' | 'inactive';
}
