import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DataSource } from 'typeorm';
import { BranchModule } from './branch/branch.module';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { Branch } from './branch/entities/branch.entity';
// import { Promotion } from './promotion/entities/promotion.entity';
import { PromotionModule } from './promotion/promotion.module';
// import { Member } from './Customer/entities/member.entity';
import { MemberModule } from './Customer/member.module';
import { ProductsModule } from './products/products.module';
// import { Product } from './products/entities/product.entity';
// import { Stock } from './stock/entities/stock.entity';
import { StockModule } from './stock/stock.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './database.sqlite',
      // entities: [Branch, Product, Promotion, Stock, Member],
      autoLoadEntities: true,
      synchronize: true,
    }),
    BranchModule,
    PromotionModule,
    MemberModule,
    ProductsModule,
    StockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
