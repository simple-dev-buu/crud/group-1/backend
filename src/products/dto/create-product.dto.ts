export class CreateProductDto {
  name: string;
  price: number;
  type: 'Coffee' | 'Bakery' | 'Food';
}
