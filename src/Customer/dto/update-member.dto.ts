import { PartialType } from '@nestjs/mapped-types';
import { CreateMemberDto } from './create-member.dto';

export class UpdateMemberDto extends PartialType(CreateMemberDto) {
  fName: string;
  lName: string;
  tel: string;
  point: number;
  birthDate: string;
}
