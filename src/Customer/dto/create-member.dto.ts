export class CreateMemberDto {
  fName: string;
  lName: string;
  tel: string;
  point: number;
  birthDate: string;
}
