export class CreateStockDto {
  code: string;
  name: string;
  price: number;
  balance: number;
  status: 'ปกติ' | 'เหลือน้อย';
}
