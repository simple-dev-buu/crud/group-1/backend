import { PartialType } from '@nestjs/mapped-types';
import { CreateStockDto } from './create-stock.dto';

export class UpdateStockDto extends PartialType(CreateStockDto) {
  code: string;
  name: string;
  price: number;
  balance: number;
  status: 'ปกติ' | 'เหลือน้อย';
}
