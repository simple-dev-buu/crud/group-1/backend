import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  code: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  balance: number;

  @Column()
  status: 'ปกติ' | 'เหลือน้อย';
}
